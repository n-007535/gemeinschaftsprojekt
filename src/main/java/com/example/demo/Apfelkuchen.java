package com.example.demo;

import javax.validation.OverridesAttribute;

import eu.hilss.kochrezepte.*;

public class Apfelkuchen extends Kuchen {

    public Apfelkuchen() {
        super();
        Apfel a = new Apfel();
        add(a, 5);
    }

    @Overrides
    public bake() {
        Ofen ofen = new Ofen();
        ofen.open();
        ofen.put(this);
        ofen.close();
        ofen.setTemperature(160);
        ofen.wait(90*60*1000);
        ofen.open();
        ofen.remove(this);
    }

    public addVanillesoße() {
        Vanillesoße sauce = new Vanillesoße();
        this.add(sauce);
    }
}
