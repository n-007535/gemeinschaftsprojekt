package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DemoApplication {

	// was bedeutet GetMapping???
	@GetMapping("/")
	String home() {
		return "Spring is here and summer is near! Yes it is";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}

// ein neues kommentar

