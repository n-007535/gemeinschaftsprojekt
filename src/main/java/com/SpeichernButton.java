package com;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

// written by ChatGPT :)
// löst das #7-Problem nicht, aber ist ja nicht so schlimm - kann jemand anders ausbaden.
public class SpeichernButton extends JFrame {

    public SpeichernButton() {
        JButton openDialogButton = new JButton("Open Dialog");
        openDialogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dialog = new JDialog(SpeichernButton.this, "Modal Dialog", true);
                JButton saveButton = new JButton("Speichern");
                dialog.add(saveButton, BorderLayout.CENTER);
                dialog.setSize(200, 150);
                dialog.setLocationRelativeTo(null);
                dialog.setVisible(true);
            }
        });

        add(openDialogButton, BorderLayout.CENTER);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 200);
        setLocationRelativeTo(null);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new SpeichernButton().setVisible(true);
            }
        });
    }
}
